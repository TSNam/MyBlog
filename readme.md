# <b>Triển khai Project Online</b>

## Bước 1: Chuẩn bị VPS Linux
1.	Đăng nhập vào VPS
- có thể Sử dụng Mobaxterm để thuận tiện sử dụng hoặc sử dụng công cụ kết từ xa mà bạn quen thuộc
2.	Cài đặt môi trường cần thiết
* Đối với PHP 
- sudo apt update
- sudo apt install apache2 mysql-server php php-mysql libapache2-mod-php php-cli php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-fpm unzip git
sudo apt install apache2 mysql-server php7.4 php-mysql libapache2-mod-php php-cli php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-fpm unzip git

## bước 2: Cài đặt Project từ git
1. Cài đặt git nếu chưa có:
- sudo apt update
- sudo apt install git
2. Di chuyển vào thư mục lưu trữ web (thường là /var/www/html):
- cd /var/www/html
3. clone dự án từ git:
- sudo git clone [đường_dẫn_git_của_bạn]
4. Di chuyển vào trong thư mục vừa clone:
- cd [tên_thư_mục]
5. Copy file cấu hình mẫu và đổi tên (Thường là .env):
- Tuỳ thuộc vào project mà bạn sử dụng để tạo file cấu hình tương ứng
6. Config file cấu hình.

### Kết nối SSH
1. Tạo một mã SSH mới (đã có thì ko cần tạo mới):
- ssh-keygen -t rsa -b 4096 -C "[email_git]"
2. Lấy mã SSH đã tạo và sử dụng:
- cat ~/.ssh/id_rsa.pub
3. Copy toàn bộ đoạn mã xuất hiện sau khi chạy lệnh bên trên và gán vào tài khoản git (thường nằm trong phần setting account).
4. Sau khi kết nối tài khoản bằng SSH thì clone, pull, push bình thường.

## bước 3: Cấu hình MySQL
1. Mở MySQL:
- sudo mysql
2. Tạo người dùng mới: (có thể dùng tài khoản root ở bước 4)
- CREATE DATABASE [Tên_database];
- CREATE USER '[Tên_người_dùng]'@'%' IDENTIFIED BY '[Your_password]';
3. Cấp quyền:
- GRANT ALL PRIVILEGES ON [Tên_database].* TO '[Tên_người_dùng]'@'%';
- FLUSH PRIVILEGES;
- EXIT;
CREATE USER 'tsnam'@'%' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON crawler.* TO 'tsnam'@'%';
### giải thích
- Thay thế localhost thành % nếu muốn public tài khoản

- CREATE USER '[Tên_người_dùng]'@'localhost' IDENTIFIED BY '[Your_password]';
* Tạo một người dùng MySQL mới có tên là "[Tên_người_dùng]" và được kết nối từ localhost (tức là chỉ có thể truy cập từ máy chủ MySQL đang chạy trên cùng máy chủ). '[Your_password]' là mật khẩu bạn chọn cho người dùng này.

- GRANT ALL PRIVILEGES ON [Tên_database].* TO '[Tên_người_dùng]'@'localhost';
* Cấp tất cả các quyền cho người dùng "[Tên_người_dùng]" trên cơ sở dữ liệu "[Tên_database]". Điều này bao gồm quyền SELECT, INSERT, UPDATE, DELETE và các quyền khác cần thiết để Project có thể hoạt động đúng cách.

- FLUSH PRIVILEGES;
*  Làm mới bảng phân quyền để đảm bảo rằng các thay đổi trong phân quyền được áp dụng ngay lập tức mà không cần phải khởi động lại MySQL.

## Bước 4: Thêm dữ liệu vào trong DB
1. Di chuyển đến thư mục chứa file SQL:
- cd [đường_dẫn_đến_thư_mục]
2. Sử dụng lệnh mysql để import file SQL:
- mysql -u [tên_người_dùng] -p [tên_cơ_sở_dữ_liệu] < [tên_file].sql
* VD: mysql -u myuser -p databasename < database.sql 

###  1 số lệnh khác:
* Hiển thị danh sách các cơ sở dữ liệu:
- SHOW DATABASES;

* Chọn cơ sở dữ liệu cụ thể:
- USE [tên_cơ_sở_dữ_liệu];

### Nên sử dụng Navicat để quản lý database
1. Tải và khởi chạy navicat
2. Ấn vào connection -> mysqsl để kết nối
3. Kết nối qua ssh; SSH -> Điền thông tin kết nối -> test connection -> Line connect SSh xanh -> Điền thông tin General -> test connection -> thông báo connect success

#### Kết nối từ xa không dùng SSH: 
1. Public Port Trên VPS:
* Đảm bảo rằng port của cơ sở dữ liệu mà bạn muốn kết nối đã được mở trên VPS. Mặc định, MySQL thường sử dụng port 3306.
* Sửa file cấu hình MySQL ([/etc/mysql/my.cnf] hoặc [/etc/my.cnf] hoặc [/etc/mysql/mysql.conf.d/mysqld.cnf]) để đảm bảo MySQL lắng nghe trên địa chỉ IP của VPS, không chỉ localhost. Bạn có thể tìm dòng sau:
- bind-address            = 127.0.0.1
* Và sửa thành:
- bind-address            = 0.0.0.0
* Sau khi sửa đổi, khởi động lại dịch vụ MySQL để áp dụng thay đổi:
- sudo service mysql restart
2. sau đó Sử dụng navicat để kết nối với DB bằng tài khoản bên trên mà ko cần ssh

#### giải thích:
* bind-address: IP mới phải khớp với địa chỉ của máy cần truy cập máy chủ MySQL từ xa. Ví dụ: nếu bạn liên kết MySQL với 0.0.0.0 thì bất kỳ máy nào truy cập máy chủ MySQL cũng có thể kết nối với nó.

* lưu ý 1: Vị trí của tệp cấu hình có thể thay đổi tùy theo bản phân phối và phiên bản đang sử dụng. Nếu tệp cấu hình MySQL không phải là vị trí mặc định của nó, hãy thử sử dụng lệnh Linuxfind để phát hiện nó.

* Lưu ý 2: Việc mở quyền truy cập từ mọi địa chỉ IP là một rủi ro bảo mật. Hãy chắc chắn rằng bạn đã thiết lập mật khẩu mạnh cho người dùng MySQL và đã áp dụng các biện pháp bảo mật khác nếu cần thiết.

- ALTER USER 'root'@'%' IDENTIFIED BY 'new_password';

## Bước 5: Cấu hình Apache
1. vô hiệu hoá cấu hình mặc định 
- sudo a2dissite 000-default
2. Tạo một tệp cấu hình mới cho Apache:
- sudo nano /etc/apache2/sites-available/[file_name].conf
3. Thêm nội dung sau vào [file_name]  (thay thế your_domain bằng tên miền hoặc địa chỉ IP của bạn):
<VirtualHost *:8080>
    ServerAdmin webmaster@your_domain
    DocumentRoot /var/www/html/[tên_thư_mục_wordpress]
    ServerName [your_domain]
    <Directory /var/www/html/[tên_thư_mục_wordpress]>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

4. Thay đổi port cho apache 
* /etc/apache2/ports.conf
* thay đổi giá trị Listen 80 thành Listen 8080
5. Di chuyển đến thư mục /etc/apache2/apache2.conf
* truy cập file và di chuyển đến cuối file thêm vào giá trị ServerName
* ServerName [your-domain]
6. Lưu và đóng tệp, sau đó kích hoạt cấu hình và khởi động lại Apache:
- sudo a2ensite [file_name].conf
- sudo systemctl restart apache2
7. Kiểm tra lại Apache để tìm lỗi cấu hình:
- sudo apachectl -t
* kết quả trả về "Syntax OK" nếu không có lỗi

-----------------------------
# 1 số lệnh khác
* dành cho apache 2, có 1 số project cần phải bật ()
- sudo a2enmod rewrite
## Thay đổi vertion PHP
vd: php 7.4
1. Thêm Repository PHP PPA:
- sudo add-apt-repository ppa:ondrej/php
- sudo apt update
2. Cài Đặt PHP 7.4 và các gói bổ sung
- sudo apt install php7.4 php7.4-cli php7.4-fpm php7.4-json php7.4-common php7.4-mysql php7.4-zip php7.4-gd php7.4-mbstring php7.4-curl php7.4-xml php7.4-bcmath php7.4-json
## cài đặt composer 
- sudo apt-get install composer
* Thêm Composer vào PATH để bạn có thể sử dụng nó từ bất kỳ thư mục nào:
- echo 'export PATH=$PATH:~/.composer/bin' >> ~/.bashrc
- composer --version
* không nên chạy với quyền root/superUser. Đọc thêm tại "getcomposer.org/root"
