<?php
/**
 * Load files.
 *
 * @package Blog_Way
 */

// Load default values.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/defaults.php';

// Custom template tags for this theme.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/template-tags.php';

// Custom functions that act independently of the theme templates.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/extras.php';

// Implement the Custom Header feature.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/custom-header.php';

// Load Jetpack compatibility file.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/jetpack.php';

// Customizer additions.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/customizer.php';

// Load hooks.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/hooks.php';

// Load widgets.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/widgets/widgets.php';

// Load dynamic css.
// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once trailingslashit( get_template_directory() ) . '/inc/dynamic.php';

if ( is_admin() ) {
	// Load about.
	// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
	require_once trailingslashit( get_template_directory() ) . 'inc/theme-info/class-about.php';
	// phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
	require_once trailingslashit( get_template_directory() ) . 'inc/theme-info/about.php';
}